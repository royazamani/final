package com.example.a00_00;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivityStudentTeacher extends AppCompatActivity implements AdapterView.OnItemSelectedListener, itemAdapter_studentteacher.itemeventlistener, FragmentAddStudentTeacher.itemeventlistenerCallback, FragmentEditStudentTeacher.itemediteventlistenercallback {
    private itemDao_studentteacher itemDaoStudentteacher;
    RecyclerView recyclerViewstudentteacher;
    private itemAdapter_studentteacher itemAdapterstudentteacher = new itemAdapter_studentteacher(this);
    MyUserManage myUserManage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainstudentteacher);
        ImageView iv_animation = findViewById(R.id.iv_animation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1, 0);
        alphaAnimation.setDuration(3500);
        alphaAnimation.setFillAfter(false);
        alphaAnimation.setRepeatCount(0);
        iv_animation.startAnimation(alphaAnimation);


        myUserManage = new MyUserManage(this);
        itemDaoStudentteacher = appdatabase_studentteacher.getAppdatabase(this).getitemDaostudentteacher();


        Spinner spinner2 = findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Menus2, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);
        spinner2.setOnItemSelectedListener(this);

        iv_animation.setVisibility(View.INVISIBLE);
        EditText search_et_studentteacher = findViewById(R.id.et_main_search_studentteacher);
        search_et_studentteacher.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    List<item_studenttecher> items = itemDaoStudentteacher.searchstudentteacher(charSequence.toString());
                    itemAdapterstudentteacher.setItemSstudentteacher(items);
                } else {
                    if (myUserManage.getGender() == "Student") {
                        List<item_studenttecher> items = itemDaoStudentteacher.getnotesstudentteacher(myUserManage.getUsername());
                        itemAdapterstudentteacher.setItemSstudentteacher(items);
                    } else {
                        List<item_studenttecher> items = itemDaoStudentteacher.getnotesstudentteacher1();
                        itemAdapterstudentteacher.setItemSstudentteacher(items);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        recyclerViewstudentteacher = findViewById(R.id.rv_main_ITEMS_studentteacher);
        LinearLayoutManager linearLayout = new LinearLayoutManager(this);
        linearLayout.setStackFromEnd(true);
        linearLayout.setReverseLayout(true);
        recyclerViewstudentteacher.setLayoutManager(linearLayout);
        recyclerViewstudentteacher.setAdapter(itemAdapterstudentteacher);


        if (myUserManage.getGender().equalsIgnoreCase("student")) {
            List<item_studenttecher> items = itemDaoStudentteacher.getnotesstudentteacher(myUserManage.getUsername());
            itemAdapterstudentteacher.addItemSstudentteacher(items);
        } else {
            List<item_studenttecher> items = itemDaoStudentteacher.getnotesstudentteacher1();
            itemAdapterstudentteacher.addItemSstudentteacher(items);
        }

        ImageView imageView = findViewById(R.id.float_plus2);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentAddStudentTeacher fragmentAdd = new FragmentAddStudentTeacher();
                fragmentAdd.show(getSupportFragmentManager(), null);
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (adapterView.getItemAtPosition(i) == adapterView.getItemAtPosition(1)) {
            Intent intent = new Intent(MainActivityStudentTeacher.this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onitemlongpress(item_studenttecher item) {
        int result = itemDaoStudentteacher.deletestudentteacher(item);
        if (result > 0) {
            itemAdapterstudentteacher.deleteItemstudentteacher(item);
        }
    }


    @Override
    public void onitemclicked(item_studenttecher item) {
        FragmentEditStudentTeacher fragmentEditstudentteacher = new FragmentEditStudentTeacher();
        Bundle bundle = new Bundle();
        bundle.putParcelable("item", item);
        fragmentEditstudentteacher.setArguments(bundle);
        fragmentEditstudentteacher.show(getSupportFragmentManager(), null);
    }


    @Override
    public void onitemcheckedchangelistener(item_studenttecher item) {
        itemDaoStudentteacher.updatestudentteacher(item);
    }

    @Override
    public void onnewItem(item_studenttecher item) {
        long itemid = itemDaoStudentteacher.addstudentteacher(item);
        if (itemid != -1) {
            item.setId(itemid);
            itemAdapterstudentteacher.addItemstudentteacher(item);
        } else {

        }
    }

    @Override
    public void onedititem(item_studenttecher item) {
        itemDaoStudentteacher.updatestudentteacher(item);
        itemAdapterstudentteacher.updateItemstudentteacher(item);

    }
}