package com.example.a00_00;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tbl_notes")
public class item implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    public long id;
    @ColumnInfo(name = "title")
    public String title;
    @ColumnInfo(name = "discript")
    public String discript;
    @ColumnInfo(name = "usernameLogin")
    public String usernamelogin;

    @ColumnInfo(name = "favorite")
    public int favorite;

    public int isFavorite() {
        return favorite;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDiscript() {
        return discript;
    }

    public void setDiscript(String discript) {
        this.discript = discript;
    }

    public String getUsernamelogin() {
        return usernamelogin;
    }

    public void setUsernamelogin(String usernamelogin) {
        this.usernamelogin = usernamelogin;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeString(this.title);
        parcel.writeString(this.discript);
        parcel.writeInt(this.favorite);
        parcel.writeString(this.usernamelogin);

    }

    public item() {
    }


    protected item(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
        this.discript = in.readString();
        this.favorite = in.readInt();
        this.usernamelogin = in.readString();

    }

    public static final Creator<item> CREATOR = new Creator<item>() {
        @Override
        public item createFromParcel(Parcel in) {
            return new item(in);
        }

        @Override
        public item[] newArray(int size) {
            return new item[size];
        }
    };
}
