
//todo in my noteDevelope App I should change my logins page font ---> primaryBold and I should initialize instance
// todo of myusermanageclass in mainActivity and write this code :
//todo     TextView tvUserName = findViewById(R.id.tvUserName);
//        AlphaAnimation alphaAnimation = new AlphaAnimation(1,0);
//        alphaAnimation.setRepeatMode(Animation.REVERSE);
//        alphaAnimation.setRepeatCount(Animation.INFINITE);
//        alphaAnimation.setDuration(1000);
//        tvUserName.startAnimation(alphaAnimation);
//        tvUserName.setText(myUserManage.getUsername());
package com.example.a00_00;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class MainActivity extends AppCompatActivity implements itemAdapter.itemeventlistener, FragmentAdd.itemeventlistenerCallback, FragmentEdit.itemediteventlistenercallback, AdapterView.OnItemSelectedListener {

    private itemDao itemDao;
    private itemAdapter itemAdapter = new itemAdapter(this);
    RecyclerView recyclerView;
    static appdatabase appdatabase;
    MyUserManage myUserManage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myUserManage = new MyUserManage(this);
        itemDao = appdatabase.getAppdatabase(this).getitemDao();
        appdatabase = Room.databaseBuilder(getApplicationContext(), appdatabase.class, "db_app")
                .allowMainThreadQueries()
                .build();

        Spinner spinner1 = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Menus1, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner1.setOnItemSelectedListener(this);


        EditText search_et = findViewById(R.id.et_main_search);
        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    if (myUserManage.getGender() == "Student") {
                        List<item> items = itemDao.getnotes(myUserManage.getUsername());
                        itemAdapter.setItemS(items);
                    } else {
                        List<item> items = itemDao.getnotes(myUserManage.getUsername());
                        itemAdapter.setItemS(items);
                    }

                    List<item> items = itemDao.search1(charSequence.toString(), myUserManage.getUsername());
                    itemAdapter.setItemS(items);
                } else {
                    if (myUserManage.getGender() == "Student") {
                        List<item> items = itemDao.getnotes(myUserManage.getUsername());
                        itemAdapter.setItemS(items);
                    } else {

                        List<item> items = itemDao.getnotes(myUserManage.getUsername());
                        itemAdapter.setItemS(items);
                    }

                    List<item> items = itemDao.search1(charSequence.toString(), myUserManage.getUsername());
                    itemAdapter.setItemS(items);


                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        recyclerView = findViewById(R.id.rv_main_ITEMS);

        LinearLayoutManager linearLayout = new LinearLayoutManager(this);
        linearLayout.setStackFromEnd(true);
        linearLayout.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setAdapter(itemAdapter);
        final List<item> items = itemDao.getnotes(myUserManage.getUsername());
        itemAdapter.addItemS(items);

        FloatingActionButton floatingActionButton = findViewById(R.id.float_plus);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentAdd fragmentAdd = new FragmentAdd();
                fragmentAdd.show(getSupportFragmentManager(), null);
            }
        });

    }

    @Override
    public void onitemlongpress(item item) {
        int result = itemDao.delete(item);
        if (result > 0) {
            itemAdapter.deleteItem(item);
        }

    }

    @Override
    public void onitemclicked(item item) {
        FragmentEdit fragmentEdit = new FragmentEdit();
        Bundle bundle = new Bundle();
        bundle.putParcelable("item", item);
        fragmentEdit.setArguments(bundle);
        fragmentEdit.show(getSupportFragmentManager(), null);

    }

    @Override
    public void onnewItem(item item) {
        long itemid = itemDao.add(item);
        if (itemid != -1) {
            item.setId(itemid);
            itemAdapter.addItem(item);
        } else {

        }
    }

    @Override
    public void onedititem(item item) {
        itemDao.update(item);
        itemAdapter.updateItem(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (adapterView.getItemAtPosition(i) == adapterView.getItemAtPosition(1)) {
            Intent intent = new Intent(MainActivity.this, MainActivityStudentTeacher.class);
            startActivity(intent);

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}