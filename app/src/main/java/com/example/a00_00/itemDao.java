package com.example.a00_00;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface itemDao {

    @Insert
    long add(item item);

    @Delete
    int delete(item item);

    @Update
    int update(item item);


    @Query("SELECT * FROM tbl_notes where usernameLogin like '%' || :usernamelogin || '%'")
    List<item> getnotes(String usernamelogin);

    @Query("SELECT * FROM TBL_NOTES WHERE title LIKE '%' || :query || '%' and usernameLogin like '%' || :usernamelogin || '%'")
    List<item> search1(String query, String usernamelogin);

    @Query("select exists (select * from tbl_notes where id=:id and favorite=1)")
    boolean isFavorite(long id);

    @Query("update tbl_notes set favorite=:state where id=:id")
    void favoriteState(long id, long state);

}
