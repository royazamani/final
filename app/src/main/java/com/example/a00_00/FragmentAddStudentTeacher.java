package com.example.a00_00;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.animation.keyframe.MaskKeyframeAnimation;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FragmentAddStudentTeacher extends DialogFragment {
    itemeventlistenerCallback itemeventlistenerCallback;
    //todo roya
    MyUserManage myUserManage;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        itemeventlistenerCallback = (FragmentAddStudentTeacher.itemeventlistenerCallback) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_add_studentteacher, null, false);
        final ImageView ivsave = view.findViewById(R.id.iv_savenote_studentteacher);
        ImageView ivback = view.findViewById(R.id.iv_back_studentteacher);
        final EditText et_new_title = view.findViewById(R.id.et_title_studentteacher);
        final TextInputLayout textInputLayout_disc = view.findViewById(R.id.textinputlayout_discript_studentteacher);
        final TextInputEditText textInputEditText_disc = view.findViewById(R.id.textinputedittext_discript_studentteacher);
        //todo roya
        myUserManage = new MyUserManage(getContext());
        String o = "";
        textInputLayout_disc.setHintAnimationEnabled(o == null);
        textInputEditText_disc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    ivsave.setImageResource(R.drawable.ic_verified_black_18dp);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        ImageView iv1 = view.findViewById(R.id.iv_first_studentteacher);
        ImageView iv2 = view.findViewById(R.id.iv_second_studentteacher);
        ImageView iv3 = view.findViewById(R.id.iv_third_studentteacher);

        AlphaAnimation alphaAnimation1 = new AlphaAnimation(1, 0);
        alphaAnimation1.setRepeatMode(Animation.REVERSE);
        alphaAnimation1.setRepeatCount(Animation.INFINITE);
        alphaAnimation1.setDuration(1500);
        iv1.startAnimation(alphaAnimation1);

        AlphaAnimation alphaAnimation2 = new AlphaAnimation(1, 0);
        alphaAnimation2.setRepeatMode(Animation.REVERSE);
        alphaAnimation2.setRepeatCount(Animation.INFINITE);
        alphaAnimation2.setDuration(2000);
        iv2.startAnimation(alphaAnimation2);

        AlphaAnimation alphaAnimation3 = new AlphaAnimation(1, 0);
        alphaAnimation3.setRepeatMode(Animation.REVERSE);
        alphaAnimation3.setRepeatCount(Animation.INFINITE);
        alphaAnimation3.setDuration(2500);
        iv3.startAnimation(alphaAnimation3);

/////////////////////////////////]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]
//        Calendar c = Calendar.getInstance();
//        SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-DD HH:MM");
//        final String formatedDate = df.format(c.getTime());

        final String currentDateTimeString = java.text.DateFormat.getDateTimeInstance().format(new Date());



        ivback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainActivityStudentTeacher.class);
                startActivity(intent);

            }
        });

        ivsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                item_studenttecher itemStudenttecher = new item_studenttecher();

                itemStudenttecher.setTitle(myUserManage.getUsername());

                itemStudenttecher.setDiscript(textInputEditText_disc.getText().toString());

                itemStudenttecher.setUsernamelogin(myUserManage.getUsername());

                itemStudenttecher.setDate(currentDateTimeString);


                itemeventlistenerCallback.onnewItem(itemStudenttecher);
                Intent intent = new Intent(getActivity(), MainActivityStudentTeacher.class);
                startActivity(intent);


            }
        });
        Intent intent2 = new Intent(getContext(), MainActivityStudentTeacher.class);
        intent2.putExtra("Time", currentDateTimeString);
//        startActivity(intent2);
        builder.setView(view);
        return builder.create();
    }

    public interface itemeventlistenerCallback {
        void onnewItem(item_studenttecher item_studenttecher);
    }
}