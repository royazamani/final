package com.example.a00_00;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface itemDao_studentteacher {

    @Insert
    long addstudentteacher(item_studenttecher item_studenttecher);

    @Delete
    int deletestudentteacher(item_studenttecher item_studenttecher);

    @Update
    int updatestudentteacher(item_studenttecher item_studenttecher);


    @Query("SELECT * FROM tbl_notes_studentteacher order by datetime desc")
    List<item_studenttecher>getnotesstudentteacher1();

    //todo roya
    @Query("SELECT * FROM tbl_notes_studentteacher  where usernameLogin like   :usernamelogin || '%' order by datetime desc")
    List<item_studenttecher>getnotesstudentteacher(String usernamelogin);



    @Query("SELECT * FROM tbl_notes_studentteacher WHERE title LIKE '%' || :query || '%'")
   List<item_studenttecher>searchstudentteacher(String query);

}
