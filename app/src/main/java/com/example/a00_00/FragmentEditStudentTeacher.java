package com.example.a00_00;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FragmentEditStudentTeacher extends DialogFragment {
    itemediteventlistenercallback itemediteventlistenercallback;
    MyUserManage myUserManage;
    private item_studenttecher item_studenttecher;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        itemediteventlistenercallback = (FragmentEditStudentTeacher.itemediteventlistenercallback) context;
        item_studenttecher = getArguments().getParcelable("item");
        if (item_studenttecher == null) {

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_edit_studentteacher, null, false);
        //todo roya
        myUserManage = new MyUserManage(getContext());
        ImageView ivback = view.findViewById(R.id.iv_back_studentteacher);
        Button btnsave = view.findViewById(R.id.btnSave_studentteacher);
        final EditText et_title = view.findViewById(R.id.et_edit_title_studentteacher);
        TextInputLayout textInputLayout_disc_edit = view.findViewById(R.id.textinputlayout_edit_discript_studentteacher);
        final TextInputEditText textInputEditText_disc_edit = view.findViewById(R.id.textinputedittext_edit_discript_studentteacher);
        et_title.setText(item_studenttecher.getTitle());
        textInputEditText_disc_edit.setText(item_studenttecher.getDiscript());
        ivback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainActivityStudentTeacher.class);
                startActivity(intent);
            }
        });
//        Calendar c = Calendar.getInstance();
//        SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-DD HH:MM");
//        final String formatedDate = df.format(c.getTime());

        final String currentDateTimeString = java.text.DateFormat.getDateTimeInstance().format(new Date());

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item_studenttecher.setTitle(et_title.getText().toString());
                item_studenttecher.setDiscript(textInputEditText_disc_edit.getText().toString());
//                item_studenttecher.setDate(formatedDate);
                //todo roya***********------------------***********************//////////////
//           ****     item_studenttecher.setUsernamelogin(myUserManage.getUsername());
                if (myUserManage.getGender().equalsIgnoreCase("admin")) {
                    item_studenttecher.setUsernamelogin(et_title.getText().toString());
                    item_studenttecher.setDate(myUserManage.getUsername() + " Answered In " + currentDateTimeString);
                } else {
                    item_studenttecher.setUsernamelogin(myUserManage.getUsername());
                    item_studenttecher.setDate("edited in " + currentDateTimeString);
                }
                itemediteventlistenercallback.onedititem(item_studenttecher);
                Intent intent = new Intent(getActivity(), MainActivityStudentTeacher.class);
                startActivity(intent);

            }
        });

        builder.setView(view);
        return builder.create();
    }

    public interface itemediteventlistenercallback {
        void onedititem(item_studenttecher item_studenttecher);
    }
}


