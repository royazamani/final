package com.example.a00_00;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(version = 2, exportSchema = false, entities = {item.class})
public abstract class appdatabase extends RoomDatabase {

    private static appdatabase appdatabase;


    public static appdatabase getAppdatabase(Context context) {
        if (appdatabase == null)
            appdatabase = Room.databaseBuilder(context.getApplicationContext(), appdatabase.class, "db_app")
                    .allowMainThreadQueries()
                    .build();
        return appdatabase;
    }

    public abstract itemDao getitemDao();
}






