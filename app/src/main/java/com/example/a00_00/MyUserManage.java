package com.example.a00_00;

import android.content.Context;
import android.content.SharedPreferences;

public class MyUserManage {
    private SharedPreferences sharedPreferences;

    public MyUserManage(Context context) {
        sharedPreferences = context.getSharedPreferences("user_information", Context.MODE_PRIVATE);
    }

    public void saveinformation(String username, String email, String gender) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("username", username);
        editor.putString("email", email);
        editor.putString("gender", gender);
        editor.apply();

    }

    public String getUsername() {
        return sharedPreferences.getString("username", "");
    }

    public String getEmail() {
        return sharedPreferences.getString("email", "");
    }

    public String getGender() {
        return sharedPreferences.getString("gender", "");
    }
}
