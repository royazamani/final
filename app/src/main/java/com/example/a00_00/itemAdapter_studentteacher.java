package com.example.a00_00;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.sql.Struct;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import static com.example.a00_00.R.drawable.mtrl_dialog_background;
import static com.example.a00_00.R.drawable.toggle_image;


public class itemAdapter_studentteacher extends RecyclerView.Adapter<itemAdapter_studentteacher.itemviewholder> {

    private List<item_studenttecher> items = new ArrayList<>();
    private itemeventlistener itemeventlistener;


    public itemAdapter_studentteacher(itemeventlistener itemeventlistener) {
        this.itemeventlistener = itemeventlistener;
    }

    @NonNull
    @Override
    public itemviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new itemviewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_studentteacher, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull itemviewholder holder, int position) {
        holder.binditem(items.get(position));

    }

    public void addItemstudentteacher(item_studenttecher item) {
        items.add(0, item);
        notifyItemInserted(0);
    }

    public void updateItemstudentteacher(item_studenttecher item) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId() == item.getId()) {

                items.set(i, item);
                notifyItemChanged(i);
                break;
            }
        }
    }

    public void deleteItemstudentteacher(item_studenttecher item) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId() == item.getId()) {
                items.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    public void addItemSstudentteacher(List<item_studenttecher> item_studenttechers) {
        this.items.addAll(item_studenttechers);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItemSstudentteacher(List<item_studenttecher> item_studenttechers) {
        this.items = item_studenttechers;
        notifyDataSetChanged();
    }

    public class itemviewholder extends RecyclerView.ViewHolder {
        private TextView tv_item_title;
        private TextView tv_item_disc;
        private TextView tv_datetime;
        private ImageView iv_fav;


        public itemviewholder(@NonNull View itemView) {
            super(itemView);
            ;
            tv_item_title = itemView.findViewById(R.id.tv_item_title__studentteacher);
            tv_item_disc = itemView.findViewById(R.id.tv_item_disc_studentteacher);
            iv_fav = itemView.findViewById(R.id.iv_favorite_studentteacher);
            tv_datetime = itemView.findViewById(R.id.tv_dateTime);


        }


        public void binditem(final item_studenttecher item) {

            iv_fav.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24);
            tv_item_title.setText(item.getTitle());
            tv_item_disc.setText(item.getDiscript());
            tv_datetime.setText(item.getDate());

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    itemeventlistener.onitemlongpress(item);
                    return false;
                }
            });
            tv_item_disc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemeventlistener.onitemclicked(item);
                }
            });
            tv_item_title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemeventlistener.onitemclicked(item);
                }
            });


        }

    }

    public interface itemeventlistener {
        void onitemlongpress(item_studenttecher item);

        void onitemclicked(item_studenttecher item);

        void onitemcheckedchangelistener(item_studenttecher item);
    }
}
