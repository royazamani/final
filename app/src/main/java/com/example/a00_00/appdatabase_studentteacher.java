package com.example.a00_00;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(version = 2, exportSchema = false, entities = {item_studenttecher.class})
public abstract class appdatabase_studentteacher extends RoomDatabase {

    private static appdatabase_studentteacher appdatabase_studentteacher;


    public static appdatabase_studentteacher getAppdatabase(Context context) {
        if (appdatabase_studentteacher == null)
            appdatabase_studentteacher = Room.databaseBuilder(context.getApplicationContext(), appdatabase_studentteacher.class, "db_app_studentteacher")
                    .allowMainThreadQueries()
                    .build();
        return appdatabase_studentteacher;
    }

    public abstract itemDao_studentteacher getitemDaostudentteacher();
}






