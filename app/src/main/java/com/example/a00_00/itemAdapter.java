package com.example.a00_00;

import android.view.DragAndDropPermissions;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;

import java.util.ArrayList;
import java.util.List;


public class itemAdapter extends RecyclerView.Adapter<itemAdapter.itemviewholder> {

    private List<item> items = new ArrayList<>();
    private itemeventlistener itemeventlistener;

    public itemAdapter(itemeventlistener itemeventlistener) {
        this.itemeventlistener = itemeventlistener;
    }

    @NonNull
    @Override
    public itemviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new itemviewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final itemviewholder holder, final int position) {
        holder.binditem(items.get(position));
    }

    public void addItem(item item) {
        items.add(0, item);
        notifyItemInserted(0);
    }


    public void updateItem(item item) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId() == item.getId()) {

                items.set(i, item);
                notifyItemChanged(i);
                break;
            }
        }
    }

    public void deleteItem(item item) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId() == item.getId()) {
                items.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    public void addItemS(List<item> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItemS(List<item> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    public class itemviewholder extends RecyclerView.ViewHolder {
        private TextView tv_item_title;
        private TextView tv_item_disc;
        private ImageView ivDelete;
        private ImageView iv_fav;


        public itemviewholder(@NonNull View itemView) {
            super(itemView);
            tv_item_title = itemView.findViewById(R.id.tv_item_title__studentteacher);
            tv_item_disc = itemView.findViewById(R.id.tv_item_disc_studentteacher);
            ivDelete = itemView.findViewById(R.id.iv_delete);
            iv_fav = itemView.findViewById(R.id.iv_favorite);

        }

        public void binditem(final item item) {
            final LottieAnimationView lottieAnimationView = itemView.findViewById(R.id.likefav_anim);

            tv_item_title.setText(item.getTitle());
            tv_item_disc.setText(item.getDiscript());

            if (item.isFavorite() == 1) {
                iv_fav.setImageResource(R.drawable.ic_baseline_favorite_24);
                lottieAnimationView.playAnimation();
            } else {
                iv_fav.setImageResource(R.drawable.ic_baseline_favorite_border_24);
            }


            iv_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (MainActivity.appdatabase.getitemDao().isFavorite(item.getId())) {
                        MainActivity.appdatabase.getitemDao().favoriteState(item.getId(), 0);
                        iv_fav.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                    } else {
                        iv_fav.setImageResource(R.drawable.ic_baseline_favorite_24);
                        MainActivity.appdatabase.getitemDao().favoriteState(item.getId(), 1);
                        lottieAnimationView.playAnimation();
                    }
                }
            });


            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemeventlistener.onitemlongpress(item);
                }
            });
            tv_item_disc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemeventlistener.onitemclicked(item);
                }
            });
            tv_item_title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemeventlistener.onitemclicked(item);
                }
            });
        }

    }

    public interface itemeventlistener {
        void onitemlongpress(item item);

        void onitemclicked(item item);


    }
}
