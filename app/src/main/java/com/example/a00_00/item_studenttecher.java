package com.example.a00_00;

import android.icu.lang.UCharacter;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.method.DateTimeKeyListener;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.sql.Date;
import java.sql.Time;

@Entity(tableName = "tbl_notes_studentteacher")
public class item_studenttecher implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    public long id;
    @ColumnInfo(name = "title")
    public String title;
    @ColumnInfo(name = "discript")
    public String discript;
    @ColumnInfo(name = "favorite")
    public boolean isfavorite;
    @ColumnInfo(name = "datetime")
    public String date ;
    @ColumnInfo(name = "usernameLogin")
    public String usernamelogin;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isIsfavorite() {
        return isfavorite;
    }

    public void setIsfavorite(boolean isfavorite) {
        this.isfavorite = isfavorite;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDiscript() {
        return discript;
    }

    public void setDiscript(String discript) {
        this.discript = discript;
    }

    public String getUsernamelogin() {
        return usernamelogin;
    }

    public void setUsernamelogin(String usernamelogin) {
        this.usernamelogin = usernamelogin;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeString(this.title);
        parcel.writeString(this.discript);
        parcel.writeByte(this.isfavorite ? (byte) 1 : (byte) 0);
        parcel.writeString(this.usernamelogin);
        parcel.writeString(this.date);

    }

    public item_studenttecher() {
    }

    protected item_studenttecher(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
        this.discript = in.readString();
        this.isfavorite = in.readByte() != 0;
        this.usernamelogin = in.readString();
        this.date=in.readString();

    }

    public static final Creator<item_studenttecher> CREATOR = new Creator<item_studenttecher>() {
        @Override
        public item_studenttecher createFromParcel(Parcel in) {
            return new item_studenttecher(in);
        }

        @Override
        public item_studenttecher[] newArray(int size) {
            return new item_studenttecher[size];
        }
    };
}
