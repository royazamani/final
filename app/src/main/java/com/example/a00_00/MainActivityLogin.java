package com.example.a00_00;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.sdsmdg.harjot.rotatingtext.RotatingTextWrapper;
import com.sdsmdg.harjot.rotatingtext.models.Rotatable;

import java.util.Calendar;
import java.util.Date;

public class MainActivityLogin extends AppCompatActivity {
    private MyUserManage myUserManage;
    private String gender = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);
        myUserManage = new MyUserManage(this);




        LottieAnimationView lottieAnimationView = findViewById(R.id.lottieanim);
        lottieAnimationView.playAnimation();

        LottieAnimationView lottieAnimationView1 = findViewById(R.id.lottieanimMessage);
        lottieAnimationView1.playAnimation();

        final TextInputEditText et_username = findViewById(R.id.et_username_shared);
        et_username.setText(myUserManage.getUsername());
        if (!(myUserManage.getUsername().length() > 0)) {
            et_username.setError("Please Enter Username");
        }

        final TextInputEditText et_email = findViewById(R.id.et_email_shared);
        et_email.setText(myUserManage.getEmail());
        if (!(myUserManage.getEmail().length() > 0)) {
            et_email.setError("Please enter Email");
        }


        Button save_btn = findViewById(R.id.btn_save_shared);

        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        gender = myUserManage.getGender();
        if (gender.equalsIgnoreCase("Student")) {
            radioGroup.check(R.id.radiobtn_student);

        } else if (gender.equalsIgnoreCase("Admin")) {
            radioGroup.check(R.id.radiobtn_admin);
        }


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.radiobtn_student) {
                    gender = "Student";
                } else {
                    gender = "Admin";
                }
            }
        });
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myUserManage.saveinformation(et_username.getText().toString(),
                        et_email.getText().toString(),
                        gender);
                Intent intent = new Intent(MainActivityLogin.this, MainActivity.class);
                startActivity(intent);

                Toast.makeText(MainActivityLogin.this, "Welcome " + myUserManage.getUsername(), Toast.LENGTH_LONG).show();
            }


        });
    }
}