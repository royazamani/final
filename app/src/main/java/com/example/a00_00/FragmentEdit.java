package com.example.a00_00;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class FragmentEdit extends DialogFragment {
    itemediteventlistenercallback itemediteventlistenercallback;
    MyUserManage myUserManage;
    private item item;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        itemediteventlistenercallback = (FragmentEdit.itemediteventlistenercallback) context;
        item = getArguments().getParcelable("item");
        if (item == null) {

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_edit, null, false);
        //todo roya
        myUserManage = new MyUserManage(getContext());
        ImageView ivback = view.findViewById(R.id.iv_back);
        Button btnsave = view.findViewById(R.id.btnSave);
        final EditText et_title = view.findViewById(R.id.et_edit_title);
        TextInputLayout textInputLayout_disc_edit = view.findViewById(R.id.textinputlayout_edit_discript);
        final TextInputEditText textInputEditText_disc_edit = view.findViewById(R.id.textinputedittext_edit_discript);
        et_title.setText(item.getTitle());
        textInputEditText_disc_edit.setText(item.getDiscript());
        ivback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item.setTitle(et_title.getText().toString());
                item.setDiscript(textInputEditText_disc_edit.getText().toString());
                //todo roya
                item.setUsernamelogin(myUserManage.getUsername());
                itemediteventlistenercallback.onedititem(item);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);

            }
        });

        builder.setView(view);
        return builder.create();
    }

    public interface itemediteventlistenercallback {
        void onedititem(item item);
    }
}


