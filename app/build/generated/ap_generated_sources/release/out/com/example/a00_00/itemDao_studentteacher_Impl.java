package com.example.a00_00;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class itemDao_studentteacher_Impl implements itemDao_studentteacher {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<item_studenttecher> __insertionAdapterOfitem_studenttecher;

  private final EntityDeletionOrUpdateAdapter<item_studenttecher> __deletionAdapterOfitem_studenttecher;

  private final EntityDeletionOrUpdateAdapter<item_studenttecher> __updateAdapterOfitem_studenttecher;

  public itemDao_studentteacher_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfitem_studenttecher = new EntityInsertionAdapter<item_studenttecher>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `tbl_notes_studentteacher` (`id`,`title`,`discript`,`favorite`,`datetime`,`usernameLogin`) VALUES (nullif(?, 0),?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, item_studenttecher value) {
        stmt.bindLong(1, value.id);
        if (value.title == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.title);
        }
        if (value.discript == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.discript);
        }
        final int _tmp;
        _tmp = value.isfavorite ? 1 : 0;
        stmt.bindLong(4, _tmp);
        stmt.bindLong(5, value.date);
        if (value.usernamelogin == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.usernamelogin);
        }
      }
    };
    this.__deletionAdapterOfitem_studenttecher = new EntityDeletionOrUpdateAdapter<item_studenttecher>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `tbl_notes_studentteacher` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, item_studenttecher value) {
        stmt.bindLong(1, value.id);
      }
    };
    this.__updateAdapterOfitem_studenttecher = new EntityDeletionOrUpdateAdapter<item_studenttecher>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `tbl_notes_studentteacher` SET `id` = ?,`title` = ?,`discript` = ?,`favorite` = ?,`datetime` = ?,`usernameLogin` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, item_studenttecher value) {
        stmt.bindLong(1, value.id);
        if (value.title == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.title);
        }
        if (value.discript == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.discript);
        }
        final int _tmp;
        _tmp = value.isfavorite ? 1 : 0;
        stmt.bindLong(4, _tmp);
        stmt.bindLong(5, value.date);
        if (value.usernamelogin == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.usernamelogin);
        }
        stmt.bindLong(7, value.id);
      }
    };
  }

  @Override
  public long addstudentteacher(final item_studenttecher item_studenttecher) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfitem_studenttecher.insertAndReturnId(item_studenttecher);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int deletestudentteacher(final item_studenttecher item_studenttecher) {
    __db.assertNotSuspendingTransaction();
    int _total = 0;
    __db.beginTransaction();
    try {
      _total +=__deletionAdapterOfitem_studenttecher.handle(item_studenttecher);
      __db.setTransactionSuccessful();
      return _total;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int updatestudentteacher(final item_studenttecher item_studenttecher) {
    __db.assertNotSuspendingTransaction();
    int _total = 0;
    __db.beginTransaction();
    try {
      _total +=__updateAdapterOfitem_studenttecher.handle(item_studenttecher);
      __db.setTransactionSuccessful();
      return _total;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<item_studenttecher> getnotesstudentteacher1() {
    final String _sql = "SELECT * FROM tbl_notes_studentteacher order by datetime desc";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "title");
      final int _cursorIndexOfDiscript = CursorUtil.getColumnIndexOrThrow(_cursor, "discript");
      final int _cursorIndexOfIsfavorite = CursorUtil.getColumnIndexOrThrow(_cursor, "favorite");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "datetime");
      final int _cursorIndexOfUsernamelogin = CursorUtil.getColumnIndexOrThrow(_cursor, "usernameLogin");
      final List<item_studenttecher> _result = new ArrayList<item_studenttecher>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final item_studenttecher _item;
        _item = new item_studenttecher();
        _item.id = _cursor.getLong(_cursorIndexOfId);
        _item.title = _cursor.getString(_cursorIndexOfTitle);
        _item.discript = _cursor.getString(_cursorIndexOfDiscript);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfIsfavorite);
        _item.isfavorite = _tmp != 0;
        _item.date = _cursor.getInt(_cursorIndexOfDate);
        _item.usernamelogin = _cursor.getString(_cursorIndexOfUsernamelogin);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<item_studenttecher> getnotesstudentteacher(final String usernamelogin) {
    final String _sql = "SELECT * FROM tbl_notes_studentteacher  where usernameLogin like   ? || '%' order by datetime desc";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (usernamelogin == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, usernamelogin);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "title");
      final int _cursorIndexOfDiscript = CursorUtil.getColumnIndexOrThrow(_cursor, "discript");
      final int _cursorIndexOfIsfavorite = CursorUtil.getColumnIndexOrThrow(_cursor, "favorite");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "datetime");
      final int _cursorIndexOfUsernamelogin = CursorUtil.getColumnIndexOrThrow(_cursor, "usernameLogin");
      final List<item_studenttecher> _result = new ArrayList<item_studenttecher>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final item_studenttecher _item;
        _item = new item_studenttecher();
        _item.id = _cursor.getLong(_cursorIndexOfId);
        _item.title = _cursor.getString(_cursorIndexOfTitle);
        _item.discript = _cursor.getString(_cursorIndexOfDiscript);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfIsfavorite);
        _item.isfavorite = _tmp != 0;
        _item.date = _cursor.getInt(_cursorIndexOfDate);
        _item.usernamelogin = _cursor.getString(_cursorIndexOfUsernamelogin);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<item_studenttecher> searchstudentteacher(final String query) {
    final String _sql = "SELECT * FROM tbl_notes_studentteacher WHERE title LIKE '%' || ? || '%'";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (query == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, query);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "title");
      final int _cursorIndexOfDiscript = CursorUtil.getColumnIndexOrThrow(_cursor, "discript");
      final int _cursorIndexOfIsfavorite = CursorUtil.getColumnIndexOrThrow(_cursor, "favorite");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "datetime");
      final int _cursorIndexOfUsernamelogin = CursorUtil.getColumnIndexOrThrow(_cursor, "usernameLogin");
      final List<item_studenttecher> _result = new ArrayList<item_studenttecher>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final item_studenttecher _item;
        _item = new item_studenttecher();
        _item.id = _cursor.getLong(_cursorIndexOfId);
        _item.title = _cursor.getString(_cursorIndexOfTitle);
        _item.discript = _cursor.getString(_cursorIndexOfDiscript);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfIsfavorite);
        _item.isfavorite = _tmp != 0;
        _item.date = _cursor.getInt(_cursorIndexOfDate);
        _item.usernamelogin = _cursor.getString(_cursorIndexOfUsernamelogin);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
