package com.example.a00_00;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class appdatabase_studentteacher_Impl extends appdatabase_studentteacher {
  private volatile itemDao_studentteacher _itemDaoStudentteacher;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(2) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `tbl_notes_studentteacher` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `title` TEXT, `discript` TEXT, `favorite` INTEGER NOT NULL, `datetime` INTEGER NOT NULL, `usernameLogin` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '511184e5339fab8682218e2fef1bcc7a')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `tbl_notes_studentteacher`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsTblNotesStudentteacher = new HashMap<String, TableInfo.Column>(6);
        _columnsTblNotesStudentteacher.put("id", new TableInfo.Column("id", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTblNotesStudentteacher.put("title", new TableInfo.Column("title", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTblNotesStudentteacher.put("discript", new TableInfo.Column("discript", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTblNotesStudentteacher.put("favorite", new TableInfo.Column("favorite", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTblNotesStudentteacher.put("datetime", new TableInfo.Column("datetime", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTblNotesStudentteacher.put("usernameLogin", new TableInfo.Column("usernameLogin", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysTblNotesStudentteacher = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesTblNotesStudentteacher = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoTblNotesStudentteacher = new TableInfo("tbl_notes_studentteacher", _columnsTblNotesStudentteacher, _foreignKeysTblNotesStudentteacher, _indicesTblNotesStudentteacher);
        final TableInfo _existingTblNotesStudentteacher = TableInfo.read(_db, "tbl_notes_studentteacher");
        if (! _infoTblNotesStudentteacher.equals(_existingTblNotesStudentteacher)) {
          return new RoomOpenHelper.ValidationResult(false, "tbl_notes_studentteacher(com.example.a00_00.item_studenttecher).\n"
                  + " Expected:\n" + _infoTblNotesStudentteacher + "\n"
                  + " Found:\n" + _existingTblNotesStudentteacher);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "511184e5339fab8682218e2fef1bcc7a", "1704f8b26d2e0a30128732d68a9a89df");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "tbl_notes_studentteacher");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `tbl_notes_studentteacher`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public itemDao_studentteacher getitemDaostudentteacher() {
    if (_itemDaoStudentteacher != null) {
      return _itemDaoStudentteacher;
    } else {
      synchronized(this) {
        if(_itemDaoStudentteacher == null) {
          _itemDaoStudentteacher = new itemDao_studentteacher_Impl(this);
        }
        return _itemDaoStudentteacher;
      }
    }
  }
}
