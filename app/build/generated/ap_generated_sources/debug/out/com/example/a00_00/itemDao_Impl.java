package com.example.a00_00;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class itemDao_Impl implements itemDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<item> __insertionAdapterOfitem;

  private final EntityDeletionOrUpdateAdapter<item> __deletionAdapterOfitem;

  private final EntityDeletionOrUpdateAdapter<item> __updateAdapterOfitem;

  private final SharedSQLiteStatement __preparedStmtOfFavoriteState;

  public itemDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfitem = new EntityInsertionAdapter<item>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `tbl_notes` (`id`,`title`,`discript`,`usernameLogin`,`favorite`) VALUES (nullif(?, 0),?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, item value) {
        stmt.bindLong(1, value.id);
        if (value.title == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.title);
        }
        if (value.discript == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.discript);
        }
        if (value.usernamelogin == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.usernamelogin);
        }
        stmt.bindLong(5, value.favorite);
      }
    };
    this.__deletionAdapterOfitem = new EntityDeletionOrUpdateAdapter<item>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `tbl_notes` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, item value) {
        stmt.bindLong(1, value.id);
      }
    };
    this.__updateAdapterOfitem = new EntityDeletionOrUpdateAdapter<item>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `tbl_notes` SET `id` = ?,`title` = ?,`discript` = ?,`usernameLogin` = ?,`favorite` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, item value) {
        stmt.bindLong(1, value.id);
        if (value.title == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.title);
        }
        if (value.discript == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.discript);
        }
        if (value.usernamelogin == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.usernamelogin);
        }
        stmt.bindLong(5, value.favorite);
        stmt.bindLong(6, value.id);
      }
    };
    this.__preparedStmtOfFavoriteState = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "update tbl_notes set favorite=? where id=?";
        return _query;
      }
    };
  }

  @Override
  public long add(final item item) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfitem.insertAndReturnId(item);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int delete(final item item) {
    __db.assertNotSuspendingTransaction();
    int _total = 0;
    __db.beginTransaction();
    try {
      _total +=__deletionAdapterOfitem.handle(item);
      __db.setTransactionSuccessful();
      return _total;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public int update(final item item) {
    __db.assertNotSuspendingTransaction();
    int _total = 0;
    __db.beginTransaction();
    try {
      _total +=__updateAdapterOfitem.handle(item);
      __db.setTransactionSuccessful();
      return _total;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void favoriteState(final long id, final long state) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfFavoriteState.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, state);
    _argIndex = 2;
    _stmt.bindLong(_argIndex, id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfFavoriteState.release(_stmt);
    }
  }

  @Override
  public List<item> getnotes(final String usernamelogin) {
    final String _sql = "SELECT * FROM tbl_notes where usernameLogin like '%' || ? || '%'";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (usernamelogin == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, usernamelogin);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "title");
      final int _cursorIndexOfDiscript = CursorUtil.getColumnIndexOrThrow(_cursor, "discript");
      final int _cursorIndexOfUsernamelogin = CursorUtil.getColumnIndexOrThrow(_cursor, "usernameLogin");
      final int _cursorIndexOfFavorite = CursorUtil.getColumnIndexOrThrow(_cursor, "favorite");
      final List<item> _result = new ArrayList<item>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final item _item;
        _item = new item();
        _item.id = _cursor.getLong(_cursorIndexOfId);
        _item.title = _cursor.getString(_cursorIndexOfTitle);
        _item.discript = _cursor.getString(_cursorIndexOfDiscript);
        _item.usernamelogin = _cursor.getString(_cursorIndexOfUsernamelogin);
        _item.favorite = _cursor.getInt(_cursorIndexOfFavorite);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<item> search1(final String query, final String usernamelogin) {
    final String _sql = "SELECT * FROM TBL_NOTES WHERE title LIKE '%' || ? || '%' and usernameLogin like '%' || ? || '%'";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 2);
    int _argIndex = 1;
    if (query == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, query);
    }
    _argIndex = 2;
    if (usernamelogin == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, usernamelogin);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "title");
      final int _cursorIndexOfDiscript = CursorUtil.getColumnIndexOrThrow(_cursor, "discript");
      final int _cursorIndexOfUsernamelogin = CursorUtil.getColumnIndexOrThrow(_cursor, "usernameLogin");
      final int _cursorIndexOfFavorite = CursorUtil.getColumnIndexOrThrow(_cursor, "favorite");
      final List<item> _result = new ArrayList<item>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final item _item;
        _item = new item();
        _item.id = _cursor.getLong(_cursorIndexOfId);
        _item.title = _cursor.getString(_cursorIndexOfTitle);
        _item.discript = _cursor.getString(_cursorIndexOfDiscript);
        _item.usernamelogin = _cursor.getString(_cursorIndexOfUsernamelogin);
        _item.favorite = _cursor.getInt(_cursorIndexOfFavorite);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public boolean isFavorite(final long id) {
    final String _sql = "select exists (select * from tbl_notes where id=? and favorite=1)";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final boolean _result;
      if(_cursor.moveToFirst()) {
        final int _tmp;
        _tmp = _cursor.getInt(0);
        _result = _tmp != 0;
      } else {
        _result = false;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
